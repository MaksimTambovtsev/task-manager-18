package ru.tsc.tambovtsev.tm.command.system;

import ru.tsc.tambovtsev.tm.api.service.ICommandService;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    public String getArgument() {
        return null;
    }

}
