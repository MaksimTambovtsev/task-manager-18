package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role roleType);

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);


}
